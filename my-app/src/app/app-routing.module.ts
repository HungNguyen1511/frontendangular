import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerComponent } from '../app/component/customer/customer.component';

const routes: Routes = [
  {
    path: "customers",
    loadChildren: () => import("./module/customer.module").then((m) => m.CustomerModule)
  },
  {
    path: "products",
    loadChildren: () => import("./module/product.module").then((m) => m.ProductModule)
  },
  {
    path: "employees",
    loadChildren: () => import("./module/employee.module").then((m) => m.EmployeeModule)
  },
  {
    path: "categories",
    loadChildren: () => import("./module/category.module").then((m) => m.CategoryModule)
  },
  {
    path: "orders",
    loadChildren: () => import("./module/order.module").then((m) => m.OrderModule)
  },
  {
    path: "orderdetails",
    loadChildren: () => import("./module/orderdetail.module").then((m) => m.OrderdetailModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
