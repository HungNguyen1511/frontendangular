import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  constructor(private http: HttpClient) { }

  getCustomer(): Observable<any>{
      return this.http.get('http://localhost:3000/orders');
  }
  getCustomerById(id : any) : Observable<any>{
    return this.http.get('http://localhost:3000/order/' + id);
  }

  addNewCustomer(customer : any) : Observable<any>{
    return this.http.post('http://localhost:3000/orders', customer);
  }

  deleteCustomer (id : any) :Observable<any>{
    return this.http.delete('http://localhost:3000/order/' + id);
  }

  editCustomer (id : any, customer : any) :Observable<any>{
    return this.http.put('http://localhost:3000/order/' + id,customer);
  }

  searchByName (name: string) : Observable<any>{
    return this.http.get('http://localhost:3000/order/' + name);
  }

}