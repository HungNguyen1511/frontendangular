import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor(private http: HttpClient) { }

  getCustomer(): Observable<any>{
      return this.http.get('http://localhost:3000/customers');
  }
  getCustomerById(id : any) : Observable<any>{
    return this.http.get('http://localhost:3000/customer/' + id);
  }

  addNewCustomer(customer : any) : Observable<any>{
    return this.http.post('http://localhost:3000/customers', customer);
  }

  deleteCustomer (id : any) :Observable<any>{
    return this.http.delete('http://localhost:3000/customer/' + id);
  }

  editCustomer (id : any, customer : any) :Observable<any>{
    return this.http.put('http://localhost:3000/customer/' + id,customer);
  }

  searchByName (name: string) : Observable<any>{
    return this.http.get('http://localhost:3000/customer/' + name);
  }

}
