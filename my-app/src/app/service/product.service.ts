import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private http: HttpClient) { }

  getProduct(): Observable<any>{
      return this.http.get('http://localhost:3000/products');
  }
  getProductById(id : any) : Observable<any>{
    return this.http.get('http://localhost:3000/product/' + id);
  }

  addNewProduct(Product : any) : Observable<any>{
    return this.http.post('http://localhost:3000/products', Product);
  }

  deleteProduct (id : any) :Observable<any>{
    return this.http.delete('http://localhost:3000/product/' + id);
  }

  editProduct (id : any, Product : any) :Observable<any>{
    return this.http.put('http://localhost:3000/product/' + id,Product);
  }

  searchByName (name: string) : Observable<any>{
    return this.http.get('http://localhost:3000/product/' + name);
  }

}