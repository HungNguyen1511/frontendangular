import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'my-app';

slideIndex : any = 1;


plusSlides(n : any) {
  this.showSlides(this.slideIndex += n);
}

currentSlide(n : any) {
  this.showSlides(this.slideIndex = n);
}
showSlides(n : any) {
  var i;
  //var slides = document.getElementsByClassName("mySlides");
  var slides = document.querySelectorAll<HTMLElement>('mySlides');
  //document.querySelectorAll<HTMLElement>('mySlides');
  // var dots = document.getElementsByClassName("dot");
  var dots = document.querySelectorAll<HTMLElement>('dot');
  if (n > slides.length) {this.slideIndex = 1}
  if (n < 1) {this.slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[this.slideIndex-1].style.display = "block";
  dots[this.slideIndex-1].className += " active";
} 

}
