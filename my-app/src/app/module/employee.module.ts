import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { EmployeeDetailComponent } from '../component/employee/employee-detail/employee-detail.component';
import { EmployeeComponent } from '../component/employee/employee.component';
import { EmployeeAddComponent } from '../component/employee/employee-add/employee-add.component';
import { EmployeeUpdateComponent } from '../component/employee/employee-update/employee-update.component';
import { EmployeeDeleteComponent } from '../component/employee/employee-delete/employee-delete.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { MaterialModule } from '../material/material.module';

const routes: Routes = [
  { path: '',
    children: [
      {
        path: '', component: EmployeeComponent,
      },
      {
        path: 'detail/:id',
        component: EmployeeDetailComponent
      },
    ],
  },
];

@NgModule({
declarations: [
    EmployeeComponent,
    EmployeeDetailComponent,
    EmployeeAddComponent,
    EmployeeUpdateComponent,
    EmployeeDeleteComponent
    ],
  imports: [RouterModule.forChild(routes),CommonModule,MaterialModule,FormsModule,ReactiveFormsModule ],
  exports: [RouterModule]
})
export class EmployeeModule { }