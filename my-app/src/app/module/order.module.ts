import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { OrderComponent } from '../component/order/order.component';
import { OrderAddComponent } from '../component/order/order-add/order-add.component';
import { OrderUpdateComponent } from '../component/order/order-update/order-update.component';
import { OrderDeleteComponent } from '../component/order/order-delete/order-delete.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { MaterialModule } from '../material/material.module';

const routes: Routes = [
  { path: '',
    children: [
      {
        path: '', component: OrderComponent,
      },
      {
        path: 'detail/:id',
        component: OrderComponent
      },
    ],
  },
];

@NgModule({
declarations: [
    OrderComponent,
    OrderAddComponent,
    OrderUpdateComponent,
    OrderDeleteComponent
    ],
  imports: [RouterModule.forChild(routes),CommonModule,MaterialModule,FormsModule,ReactiveFormsModule ],
  exports: [RouterModule]
})
export class OrderModule { }