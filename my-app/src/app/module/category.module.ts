import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { CategoryDetailComponent } from '../component/category/category-detail/category-detail.component';
import { CategoryComponent } from '../component/category/category.component';
import { CategoryAddComponent } from '../component/category/category-add/category-add.component';
import { CategoryUpdateComponent } from '../component/category/category-update/category-update.component';
import { CategoryDeleteComponent } from '../component/category/category-delete/category-delete.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { MaterialModule } from '../material/material.module';

const routes: Routes = [
  { path: '',
    children: [
      {
        path: '', component: CategoryComponent,
      },
      {
        path: 'detail/:id',
        component: CategoryDetailComponent
      },
    ],
  },
];

@NgModule({
declarations: [
    CategoryComponent,
    CategoryDetailComponent,
    CategoryAddComponent,
    CategoryUpdateComponent,
    CategoryDeleteComponent
    ],
  imports: [RouterModule.forChild(routes),CommonModule,MaterialModule,FormsModule,ReactiveFormsModule ],
  exports: [RouterModule]
})
export class CategoryModule { }