import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { ProductDetailComponent } from '../component/product/product-detail/product-detail.component';
import { ProductComponent } from '../component/product/product.component';
import { ProductAddComponent } from '../component/product/product-add/product-add.component';
import { ProductUpdateComponent } from '../component/product/product-update/product-update.component';
import { ProductDeleteComponent } from '../component/product/product-delete/product-delete.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { MaterialModule } from '../material/material.module';

const routes: Routes = [
  { path: '',
    children: [
      {
        path: '', component: ProductComponent,
      },
      {
        path: 'detail/:id',
        component: ProductDetailComponent
      },
    ],
  },
];

@NgModule({
declarations: [
    ProductComponent,
    ProductDetailComponent,
    ProductAddComponent,
    ProductUpdateComponent,
    ProductDeleteComponent
    ],
  imports: [RouterModule.forChild(routes),CommonModule,MaterialModule,FormsModule,ReactiveFormsModule ],
  exports: [RouterModule]
})
export class ProductModule { }
