import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { CustomerDetailComponent } from '../component/customer/customer-detail/customer-detail.component';
import { CustomerComponent } from '../component/customer/customer.component';
import { CustomerAddComponent } from '../component/customer/customer-add/customer-add.component';
import { CustomerUpdateComponent } from '../component/customer/customer-update/customer-update.component';
import { CustomerDeleteComponent } from '../component/customer/customer-delete/customer-delete.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { MaterialModule } from '../material/material.module';

const routes: Routes = [
  { path: '',
    children: [
      {
        path: '', component: CustomerComponent,
      },
      {
        path: 'detail/:id',
        component: CustomerDetailComponent
      },
    ],
  },
];

@NgModule({
declarations: [
    CustomerComponent,
    CustomerDetailComponent,
    CustomerAddComponent,
    CustomerUpdateComponent,
    CustomerDeleteComponent
    ],
  imports: [RouterModule.forChild(routes),CommonModule,MaterialModule,FormsModule,ReactiveFormsModule ],
  exports: [RouterModule]
})
export class CustomerModule { }
