import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { OrderdetailAddComponent } from '../component/orderdetail/orderdetail-add/orderdetail-add.component';
import { OrderdetailUpdateComponent } from '../component/orderdetail/orderdetail-update/orderdetail-update.component';
import {OrderdetailDeleteComponent } from '../component/orderdetail/orderdetail-delete/orderdetail-delete.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { OrderdetailComponent } from '../component/orderdetail/orderdetail.component';

const routes: Routes = [
  { path: '',
    children: [
      {
        path: '', component: OrderdetailComponent,
      },
      {
        path: 'detail/:id',
        component: OrderdetailComponent
      },
    ],
  },
];

@NgModule({
declarations: [
    OrderdetailComponent,
    OrderdetailAddComponent,
    OrderdetailUpdateComponent,
    OrderdetailDeleteComponent
    ],
  imports: [RouterModule.forChild(routes),CommonModule,MaterialModule,FormsModule,ReactiveFormsModule ],
  exports: [RouterModule]
})
export class OrderdetailModule { }
