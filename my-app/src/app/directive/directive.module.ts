import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightDirective } from 'src/app/directive/highlight.directive';
import { BrowserModule } from '@angular/platform-browser';
import {UnlessDirective} from '../directive/unless.directive';
  import { from } from 'rxjs';

const DIRECTIVE = [
  HighlightDirective,
  UnlessDirective
]

@NgModule({
  declarations: [
    ...DIRECTIVE,
  ],
  imports: [
    BrowserModule
  ],
  exports: [
    ...DIRECTIVE
  ],
})
export class DirectiveModule { }
