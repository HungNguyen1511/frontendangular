import { from } from 'rxjs';
import {Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';

@Directive({
    selector: '[appHighlight]'
})

export class HighlightDirective implements OnInit {


    constructor(private el: ElementRef){
    }

    ngOnInit(): void {

    }

    @HostListener ('mouseenter') onMouseEnter() {
    }

    @HostListener ('mouseleave') onMouseLeave() {
        this.el.nativeElement.style.backgroundColor  = "red";
    }

    @HostListener ('dblclick') onDblClick() {
        this.el.nativeElement.style.backgroundColor  = "dark";
    }



}
