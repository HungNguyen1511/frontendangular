import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss']
})
export class ProductAddComponent implements OnInit {

  product : any ={
  };

  constructor(
    public dialogRef: MatDialogRef<ProductAddComponent>,
    @Inject(MAT_DIALOG_DATA) public data ,
    private ProductService : ProductService,
    private toastrService: ToastrService
  ) { }
  ngOnInit(): void {
  }

  public formgroup : FormGroup;
  
  public gioiTinh : boolean = true;
  close(){
    this.dialogRef.close();
  }

  save(form){
    if (form.valid){
      this.ProductService.addNewProduct(this.product).subscribe(
        data => {
          this.toastrService.success("Them moi san pham thanh cong");
          this.dialogRef.close(true);
        },
        error =>{
          console.log(error);
        }
      )
    }
    else{
      console.log('invalid form');
    }
  }


}
