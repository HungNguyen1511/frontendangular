import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { ProductService } from 'src/app/service/product.service';
import { Product } from './product';
import { ProductAddComponent } from './product-add/product-add.component';
import { ProductUpdateComponent } from './product-update/product-update.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  public product : Product;
  public listProduct:[];
  public viewList: boolean;
  public search = new FormControl();
  observable: any;
  subject = new Subject<number>();
  behaviourSubject = new BehaviorSubject<number>(0);
  constructor(private router: Router,
    private productService: ProductService,
    public dialog: MatDialog)  { 
      this.viewList = true;
    }

  ngOnInit(): void {
    this.observable = new Observable ( observer => {
      observer.next(1);
      observer.next(2);
      observer.next(3);
      setInterval(() => {
        observer.next(4);
        // observer.complete();
      },1000);
    });

    this.subject.subscribe({
      next: (n) => console.log(`observerA: ${n}`)
    });

    this.subject.subscribe({
      next: (n) => console.log(`observerB: ${n}`)
    });

    this.behaviourSubject.subscribe({
      next: (n) => console.log(`observerC: ${n}`)
    });
    this.behaviourSubject.next(1);
    this.behaviourSubject.next(2);

    // call api
    this.productService.getProduct().subscribe(
      data => {
        console.log(data);
        this.listProduct = data;
      },
      error =>{
        console.log(error);
      }
    );
    
    this.search.valueChanges.subscribe(value => {
      console.log(value);
    })
  }

  viewDetail(id){
    this.router.navigateByUrl('product/detail/' + id);
    this.router.navigate(['product','detail',id]);
  }

  subscribe(){
    console.log('just before subscribe');
    const subscription = this.observable.subscribe({
      next(x) {console.log('got value' + x); },
      error(err) {console.error('something wrong occured: ' + err);
      },
      complete() {console.log('done');}
    });
    console.log('just after subscibe');
    
    setTimeout(() => {
      subscription.unsubscribe(); 
    }, 3000);

    this.subject.next(1);
    this.subject.next(2);

    //behaviour subject
    this.behaviourSubject.subscribe({
      next: (n) => console.log(`observerD: ${n}`)
    });
    this.behaviourSubject.next(3);
  }

  Card(){
    this.viewList = false;
  }

  List(){
    this.viewList = true;
  }

  addProduct(){
    this.productService.addNewProduct(this.product).subscribe(
      data => {
        console.log(data);
      },
      error =>{
        console.log(error);
      }
    )
  }

  deleteProduct (id){
    this.productService.deleteProduct(id).subscribe(
      data => {
        console.log(data);
      },
      error =>{
        console.log(error);
      }
    )
  }

  
  getList(){
    this.productService.getProduct().subscribe(
      data => {
        this.listProduct = data;
      },
      error => {
        console.log(error);
      }
    )
  }
  openAddProductDialog(){
    const dialogRef = this.dialog.open(ProductAddComponent, {
      panelClass: 'product-add-dialog',
      width: "500px",
      disableClose: true,
      data:{}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result){
        this.getList();
      }
      console.log('Dialog result: ${result}');
      
    });
  }

  editProduct (id){
    this.productService.editProduct(id, this.product).subscribe(
      data => {
        console.log(data);
      },
      error =>{
        console.log(error);
      }
    )
  }
  openUpdateProductDialog(event ,item ){
    event.stopPropagation();
    const dialogRef = this.dialog.open(ProductUpdateComponent,{
      panelClass: 'product-update-dialog',
      width: "500px",
      disableClose: true,
      data:{
        title: "edit",
        product: item
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result){
        this.getList();
      }
      console.log('Dialog result: ${result}');     
    });
  }
}
