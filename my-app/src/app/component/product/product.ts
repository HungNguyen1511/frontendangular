export interface Product {
    id: number;
    productName: string;
    color : string;
    description: string;
    price: number;
    discount: number;
    quantityPerUnit: number;
    unitInStock: number;
    photo: string;
    categoryID: number; 
  }
