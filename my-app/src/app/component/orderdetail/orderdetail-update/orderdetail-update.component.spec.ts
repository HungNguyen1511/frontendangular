import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderdetailUpdateComponent } from './orderdetail-update.component';

describe('OrderdetailUpdateComponent', () => {
  let component: OrderdetailUpdateComponent;
  let fixture: ComponentFixture<OrderdetailUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderdetailUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderdetailUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
