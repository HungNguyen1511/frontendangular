import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderdetailDeleteComponent } from './orderdetail-delete.component';

describe('OrderdetailDeleteComponent', () => {
  let component: OrderdetailDeleteComponent;
  let fixture: ComponentFixture<OrderdetailDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderdetailDeleteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderdetailDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
